# Heavily modified Pygmalion theme, mocking my preferred bash prompt

if [[ $COLORTERM = gnome-* && $TERM = xterm ]] && infocmp gnome-256color >/dev/null 2>&1; then
  export TERM=gnome-256color
elif infocmp xterm-256color >/dev/null 2>&1; then
  export TERM=xterm-256color
fi

if tput setaf 1 &> /dev/null; then
  tput sgr0
  if [[ $(tput colors) -ge 256 ]] 2>/dev/null; then
    MAGENTA=$(tput setaf 9)
    ORANGE=$(tput setaf 172)
    GREEN=$(tput setaf 190)
    PURPLE=$(tput setaf 141)
    WHITE=$(tput setaf 46)
    BLUE=$(tput setaf 39)
  else
    MAGENTA=$(tput setaf 5)
    ORANGE=$(tput setaf 4)
    GREEN=$(tput setaf 2)
    PURPLE=$(tput setaf 1)
    WHITE=$(tput setaf 7)
    BLUE=$(tput setaf 4)
  fi
  BOLD=$(tput bold)
  RESET=$(tput sgr0)
else
  MAGENTA="\033[1;31m"
  ORANGE="\033[1;33m"
  GREEN="\033[1;32m"
  PURPLE="\033[1;35m"
  WHITE="\033[1;37m"
  BOLD=""
  RESET="\033[m"
fi

export MAGENTA
export ORANGE
export GREEN
export PURPLE
export WHITE
export BLUE
export BOLD
export RESET

prompt_setup_cmck(){
  ZSH_THEME_GIT_PROMPT_PREFIX="%{$reset_color%}%{$WHITE%}[%{$reset_color%}%{$PURPLE%}"
  ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}%{$WHITE%}]"
  ZSH_THEME_GIT_PROMPT_DIRTY="%{$PURPLE%}*%{$reset_color%}"
  ZSH_THEME_GIT_PROMPT_CLEAN=""

  base_prompt='%{$ORANGE%}%m%{$reset_color%}%{$WHITE%}の%{$reset_color%}%{$MAGENTA%}%n%{$reset_color%}%{$WHITE%}:%{$reset_color%}%{$GREEN%}%0~%{$reset_color%}%{$reset_color%}'
  post_prompt='%{$BLUE%}⟴%{$reset_color%}  '

  base_prompt_nocolor=$(echo "$base_prompt" | perl -pe "s/%\{[^}]+\}//g")
  post_prompt_nocolor=$(echo "$post_prompt" | perl -pe "s/%\{[^}]+\}//g")

  precmd_functions+=(prompt_cmck_precmd)
}

prompt_cmck_precmd(){
  #local gitinfo=$(git_prompt_info)
  local gitinfo=$(_omz_git_prompt_info)
  local gitinfo_nocolor=$(echo "$gitinfo" | perl -pe "s/%\{[^}]+\}//g")
  local exp_nocolor="$(print -P \"$base_prompt_nocolor$gitinfo_nocolor$post_prompt_nocolor\")"
  local prompt_length=${#exp_nocolor}

  PROMPT="$base_prompt$gitinfo$post_prompt"
}

prompt_setup_cmck

