let os=substitute(system('uname'), '\n', '', '')
" Make Vim more useful
set nocompatible
" Use the OS clipboard by default (on versions compiled with `+clipboard`)
set clipboard=unnamed
" Enhance command-line completion
"set wildmenu
" Allow cursor keys in insert mode
set esckeys
" Allow backspace in insert mode
set backspace=indent,eol,start
" Optimize for fast terminal connections
set ttyfast
" Add the g flag to search/replace by default
set gdefault
" Use UTF-8 without BOM
set encoding=utf-8 nobomb
" Change mapleader
"let mapleader=","
" Don’t add empty newlines at the end of files
set binary
set noeol

let cwd = getcwd()

" Centralize backups, swapfiles and undo history
" even when running under sudo
if strlen($SUDO_USER)
  let realuser    = $SUDO_USER
else
  let realuser    = $USER
endif

let userhomedir  = substitute(system('echo ~' . realuser), '\n', '', '')
let &backupdir   = userhomedir . '/.vim/backups'
if !isdirectory(&backupdir)
  call mkdir(&backupdir, "p")
endif
let &directory   = userhomedir . '/.vim/swaps'
if !isdirectory(&directory)
  call mkdir(&directory, "p")
endif
let &undodir     = userhomedir . '/.vim/undo'
if !isdirectory(&undodir)
  call mkdir(&undodir, "p")
endif
set undofile
let &runtimepath = userhomedir . '/.vim,' . &runtimepath . ',' . userhomedir . '/.vim/after'

" Set os and user-appropriate directory for custom syntax definitions
let syntaxdir = userhomedir . '/.vim/syntax/'

if !isdirectory(userhomedir . '/.vim/pack/')
  call mkdir(userhomedir . '/.vim/pack/', "p")
endif
let packpath = userhomedir . '/.vim/pack/'

filetype plugin on

" Respect modeline in files
set modeline
set modelines=4
" Enable per-directory .vimrc files and disable unsafe commands in them
"set exrc
"set secure
" Enable line numbers
set number
" Enable syntax highlighting
syntax on
" Highlight current line
set cursorline
" Make tabs as wide as two spaces
set tabstop=2
" Use spaces for tabs, damn it
set expandtab
" Indent automatically
set smartindent
" Show “invisible” characters
set lcs=tab:▸\ ,trail:·,eol:¬,nbsp:_
set list
" Highlight searches
"set hlsearch
" Ignore case of searches
set ignorecase
" Highlight dynamically as pattern is typed
set incsearch
" Always show status line
set laststatus=2
" Enable mouse in all modes
set mouse=a
" Disable error bells
set noerrorbells
" Don’t reset cursor to start of line when moving around.

set nostartofline
" Show the cursor position
set ruler
" Don’t show the intro message when starting Vim
set shortmess=atI
" Show the current mode
set showmode
" Show the filename in the window titlebar
set title
" Show the (partial) command as it’s being typed
set showcmd
" Use relative line numbers
"if exists("&relativenumber")
"  set relativenumber
"  au BufReadPost * set relativenumber
"endif
" Start scrolling three lines before the horizontal window border
set scrolloff=3

" Hit option-D to duplicate the current line
noremap ∂ "zyy"zp
noremap d "zyy"zp

" Strip trailing whitespace (,ss)
function! StripWhitespace()
  let save_cursor = getpos(".")
  let old_query = getreg('/')
  :%s/\s\+$//e
  call setpos('.', save_cursor)
  call setreg('/', old_query)
endfunction
noremap <leader>ss :call StripWhitespace()<CR>

" Convert tabs into spaces
function! CleanUpTabs()
  let save_cursor = getpos(".")
  let old_query = getreg('/')
  :%s/\t/  /e
  :%s/\s\+$//e
  call setpos('.', save_cursor)
  call setreg('/', old_query)
endfunction

" Hit option-shift-C to clean up the buffer
" turns tabs into spaces and removes trailing white space
noremap <silent> Ç :call CleanUpTabs()<CR>
noremap <silent> C :call CleanUpTabs()<CR>

" Save a file as root (,W)
noremap <buffer> <S-w> :w !sudo tee % > /dev/null<CR>

" Take off and nuke the buffer from orbit
" (It's the only way to be sure)...
nmap <Leader>x 1GdG

" Intelligent newline
function! FancyNewLine()
  let save_cursor = getpos(".")
  let old_query = getreg('/')
  let magicline = line('.')-1
  echom magicline
  let searchlead = '/\%'
  execute 'silent normal! ' . searchlead . magicline . "l['\"#]" . "\r"
  let horizalign = col('.')
  let qchar = getline('.')[col('.')-1]
  call setpos('.', save_cursor)
  let @j = qchar
  normal k$"jpj^"jP
  call setpos('.', save_cursor)
  call setreg('/', old_query)
endfunction

noremap <Leader> i:call FancyNewLine()<CR>i

" Per-filetype commands

" Automatic commands
if has("autocmd")
  " Enable file type detection
  filetype on
  " Treat .json files as .js
  autocmd BufNewFile,BufRead *.json setfiletype json syntax=javascript
  " Give a visual indicator of the PEP8 line-length guideline
  autocmd BufNewFile,BufRead *.py match OverLength /\%161v.\+/
endif

au FileType puppet setlocal shiftwidth=2
au FileType python setlocal shiftwidth=8 tabstop=8

" Run as python and show results (Shift-P)
noremap <buffer> <Leader>p :w<CR>:!/usr/bin/env python % <CR>
" Run as ruby and show results (Option-R)
noremap <buffer>  :w<CR>:!/usr/bin/env ruby % <CR>
let g:neocomplcache_enable_at_startup = 1

" Use tab key as escape to switch modes
"nnoremap <Tab> <Esc>
"vnoremap <Tab> <Esc>gV
"onoremap <Tab> <Esc>
"inoremap <Tab> <Esc>`^
"inoremap <Leader><Tab> <Tab>
"imap <tab> <esc>

" Make crontab actually work
if $VIM_CRONTAB == 'true'
  set nobackup
  set nowritebackup
endif

" Syntax
let g:ale_linters = {
\   'python': ['pyls'],
\   'puppet': ['puppet','puppetlint'],
\   'javascript': ['eslint'],
\   'rust': ['rls'],
\   'ansible': ['ansible_lint'],
\   'yaml': ['ansible_lint'],
\}

" Python linting
let g:ale_python_pyls_config = {
            \    'pyls' :{
            \        'plugins' : {
            \            'pycodestyle': {
            \                'enabled' : v:true,
            \                'maxLineLength': 159
            \            },
            \            'autopep8': {
            \                'enabled' : v:false,
            \            },
            \        }
            \    }
            \}

" Puppet linting
let g:syntastic_puppet_checkers        = ['puppet','puppetlint']
let g:syntastic_puppet_puppetlint_args = '--no-80chars-check --no-documentation-check --no-autoloader_layout-check'
if isdirectory('/usr/local/rvm/wrappers/ruby-1.9.3-p551@global/')
  let g:ale_puppet_puppetlint_executable = '/usr/local/rvm/wrappers/ruby-1.9.3-p551@global/puppet-lint'
else
  let g:ale_puppet_puppetlint_executable = '/usr/local/bin/puppet-lint'
endif

" JS Linting
let g:syntastic_javascript_checkers    = ['eslint']
if filereadable(findfile('node_modules/eslint/bin/eslint.js', cwd . ';'))
  let eslint_bin = findfile('node_modules/eslint/bin/eslint.js', cwd . ';')
else
  let eslint_bin = 'eslint'
endif
if filereadable(findfile('.eslintrc', cwd . ';'))
  let eslint_rc_file = findfile('.eslintrc', cwd . ';')
elseif filereadable(findfile('.eslintrc.js', cwd . ';'))
  let eslint_rc_file = findfile('.eslintrc.js', cwd . ';')
elseif filereadable(findfile('.eslintrc.cjs', cwd . ';'))
  let eslint_rc_file = findfile('.eslintrc.cjs', cwd . ';')
else
  let eslint_rc_file = userhomedir . '/.eslintrc'
endif
let g:syntastic_javascript_eslint_args = '-c ' . eslint_rc_file
let g:ale_javascript_eslint_options = '-c ' . eslint_rc_file
let g:syntastic_javascript_eslint_exec = eslint_bin
let g:ale_javascript_eslint_executable = eslint_bin

" Rust Linting
let g:ale_rust_rls_executable = '/Users/carter/.cargo/bin/rls'
let g:syntastic_check_on_open = 1

" Ansible support
au BufRead,BufNewFile */playbooks/*.yml set filetype=yaml.ansible
"let g:ale_command_wrapper = 'docker run --rm -v /Users/carter/projects/dibs-ansible:/Users/carter/projects/dibs-ansible 268215509542.dkr.ecr.us-east-1.amazonaws.com/dibs-ansible:latest'

" Folding
set foldmethod=indent
set foldcolumn=3
set foldlevelstart=99
setlocal shiftwidth=4
highlight Folded ctermfg=DarkGreen ctermbg=Black
nnoremap <space> za
vnoremap <space> zf

" YouCompleteMe
"let g:ycm_key_list_select_completion                = ['<S-TAB>', 'Enter', '<Down>']
"let g:ycm_key_list_previous_completion              = ['<Up>']
"let g:ycm_autoclose_preview_window_after_completion = 1

" Sometimes relative line numbers are useful
function! NumberToggle()
  if(&relativenumber == 1)
    set relativenumber!
    set number
  else
    set number!
    set relativenumber
  endif
endfunc

nnoremap ˜ :call NumberToggle()<cr>
nnoremap N :call NumberToggle()<cr>

" Inserting useful, dynamic filler text
nnoremap Ò :r !curl -s http://loripsum.net/api/plaintext/prude<cr>
nnoremap L :r !curl -s http://loripsum.net/api/plaintext/prude<cr>

let g:EasyMotion_leader_key = '<S-e>'

" Maybe
command! -complete=shellcmd -nargs=+ Shell call s:RunShellCommand(<q-args>)
function! s:RunShellCommand(cmdline)
  echo a:cmdline
  let expanded_cmdline = a:cmdline
  for part in split(a:cmdline, ' ')
     if part[0] =~ '\v[%#<]'
        let expanded_part = fnameescape(expand(part))
        let expanded_cmdline = substitute( expanded_cmdline, part, expanded_part, '')
     endif
  endfor
  botright new
  setlocal buftype=nofile bufhidden=wipe nobuflisted noswapfile nowrap
  call setline(1, 'You entered:    ' . a:cmdline)
  call setline(2, 'Expanded Form:  ' .expanded_cmdline)
  call setline(3,substitute(getline(2),'.','=','g'))
  execute '$read !'. expanded_cmdline
  setlocal nomodifiable
  1
endfunction

" Make gitgutter calm down
let g:gitgutter_realtime = 0
let g:gitgutter_eager    = 0

" Use UltiSnips but make it leave Tab alone
let g:UltiSnipsSnippetDirectories = [ "UltiSnips" ]
let g:UltiSnipsSnippetsDir        = userhomedir . '/.vim/snippets/'
"let g:UltiSnipsExpandTrigger      = "≈"
let g:UltiSnipsExpandTrigger      = "<tab>"
let g:ycm_key_list_select_completion=[]
let g:ycm_key_list_previous_completion=[]

" Airline
let g:airline_powerline_fonts = 0
let g:airline_symbols         = {}
" unicode symbols
let g:airline_left_sep           = '▶'
let g:airline_right_sep          = '◀'
let g:airline_symbols.linenr     = 'Þ'
let g:airline_symbols.branch     = '⎇'
let g:airline_symbols.paste      = 'ρ'
let g:airline_symbols.whitespace = 'Ξ'
function! AirlineInit()
  let g:airline_section_y = airline#section#create(['ffenc', '%{strftime("%H:%M:%S")}'])
endfunction
autocmd VimEnter * call AirlineInit()

" Lines that are too long get colored red
highlight OverLength ctermbg = red ctermfg = white guibg = #592929

" Don't use arrow keys
noremap OA <Nop>
noremap <Up> <Nop>
noremap OB <Nop>
noremap <Left> <Nop>
noremap OC <Nop>
noremap <Down> <Nop>
noremap OD <Nop>
noremap <Right> <Nop>

" VCL highlighting
au BufRead,BufNewFile *.vcl :set ft=vcl
exec 'au! Syntax vcl source '.syntaxdir . 'vcl.vim'

inoremap # X#

let g:tagbar_type_puppet = {
  \ 'ctagstype': 'puppet',
  \ 'kinds': [
    \'c:class',
    \'s:site',
    \'n:node',
    \'d:definition',
    \'r:resource',
    \'f:default'
  \]
\}

" NerdTree stuff
let NERDTreeMinimalUI=1

let g:mta_filetypes = {
    \ 'html' : 1,
    \ 'xhtml' : 1,
    \ 'xml' : 1,
    \ 'eruby' : 1,
    \}

function! ExpandVar(my_line)
  echo a:my_line
  let save_pos = getpos(".")
  let x = split(substitute(getline(a:my_line),"\"","","g"),'=')
  echo x
  execute ":%s/\$" . x[0] . "/" . x[1] "/g"
  call setpos('.', save_pos)
endfunction

" Transition to serverless reporting
function! Reporting()
  :g/_em\(ai\)\?l=/call ExpandVar(line('.'))
  :v/^\s*spcall\|^\s*email\|^\s*mysql\|^\s*# [A-Z0-9-]\|^$/d
  :g/^\s*email/s/[^"]*"[^"]*" "\([^\$]*\)[^"]*" "\([^"]*\)" "\$.*\/\([^\$]*\).*/      "Recipients": "\2",      "FilenamePrefix": "\3",      "EmailSubjectPrefix": "\1"/
  :g/^\s*spcall /s/.*spcall "\([^"]*\).*/      "StoredProc": "reporting.\1",/
  :g/^\s*spcallparm /s/.*spcallparm "\([^"]*\)" "[^"]*" "'\([^']*\)'".*/      "StoredProc": "reporting.\1",      "StoredProcParameters": "\2",/
  :g/^\s*spcall2 /s/.*spcall2 "\([^()]*\)(\([^)]*\)).*/      "StoredProc": "reporting.\1",^M      "StoredProcParameters": "\2",/
  :g/^\s*mysql /s/.*CALL \([^(]*\)('\?\([^')]*\).*/      "StoredProc": "\1",      "StoredProcParameters": "\2",/
  :g/^\s*# /s/[^A-Za-z0-9]//
  :g/^[A-Z0-9]/s/\([A-Za-z0-9]*\)/    "\1": {/
  :%s/^$/    },/
endfunction

let g:pymode_python = 'python3'
let g:pymode_options_max_line_length = 159

" If the file has a script-like name suffix then set the executable bit upon
" saving
function! SetExecutableBit()
  " List of script-like file extensions
  let l:script_exts = ['sh', 'bash', 'zsh', 'csh', 'py', 'pl', 'rb', 'js', 'php', 'lua']

  " Get the current buffer's file extension
  let l:ext = expand('%:e')

  " Check if the file extension is in the list of script-like extensions
  if index(l:script_exts, l:ext) >= 0
    " Set the executable bit using the system's chmod command
    silent !chmod +x %
    echo "Set executable bit on" expand('%')
  endif
endfunction

" Create an autocommand to run the function on buffer write
autocmd BufWritePost * call SetExecutableBit()

" Autosave on lost focus
:au FocusLost * silent! wa

