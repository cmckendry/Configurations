# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="cmck"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM="$HOME/.zsh_custom"

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

## Ported from bash profile

unamestr=`uname`

if [ "$EUID" -ne 0 ]; then
    SUDO="/usr/bin/sudo "
else
    SUDO=""
fi

if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    OS=$(lsb_release -si)
    VER=$(lsb_release -sr)
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS=Debian
    VER=$(cat /etc/debian_version)
elif [ -f /etc/SuSe-release ]; then
    # Older SuSE/etc.
    OS=SuSe
elif [ -f /etc/redhat-release ]; then
    # Older Red Hat, CentOS, etc.
    OS=RHEL
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
fi


# Load the shell dotfiles, and then some:
# * ~/.path can be used to extend `$PATH`.
# * ~/.extra can be used for other settings you don’t want to commit.
for file in ~/.{path,bash_prompt,exports,aliases,functions,extra}; do
  [ -r "$file" ] && source "$file"
done
unset file

# Unicode settings needed for linux
export LANG=en_US.UTF8
export GDM_LANG=en_US.UTF8

# Case-insensitive globbing (used in pathname expansion)
unsetopt CASE_GLOB

# Preserve real-time bash history in multiple terminal windows
export HISTCONTROL=ignoredups:erasedups  # no duplicate entries
export HISTSIZE=100000                   # big big history
export HISTFILESIZE=100000               # big big history
setopt inc_append_history
setopt share_history

# MacPorts Installer addition on 2012-10-12_at_15:58:56: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH

# Prevent the SUPER ANNOYING "temp file must be edited in place" error from crontab
alias crontab="VIM_CRONTAB=true crontab"

#alias wget="curl -O"

function vpn-connect {
echo "tell application \"System Events\"
  tell current location of network preferences
    set VPN to service \"1stDibs\" -- your VPN name here
    if exists VPN then connect VPN
    repeat while (current configuration of VPN is not connected)
      delay 1
    end repeat
  end tell
end tell" | /usr/bin/env osascript
}

function vpn-disconnect {
"tell application \"System Events\"
  tell current location of network preferences
    set VPN to service \"1stDibs\" -- your VPN name here
    if exists VPN then disconnect VPN
  end tell
end tell" | /usr/bin/env osascript
}

# Cache that locked ssh private key!
eval `ssh-agent` > /dev/null
ssh-add ~/.ssh/id_rsa

if ping -c 1 -W 2 8.8.8.8 > /dev/null; then
    cd ~/Configurations && git remote set-url origin git@gitlab.com:cmckendry/Configurations.git
    cd ~/Configurations && (git pull --rebase &) &> /dev/null && (git submodule update --init --recursive) &> /dev/null
    cd
    if [ -f ~/Configurations/secrets/.secrets ]; then
        cd ~/Configurations/secrets && git remote set-url origin git@gitlab.com:cmckendry/secrets.git
        git pull --rebase
        cd
        source ~/Configurations/secrets/.secrets
    else
        git clone git@gitlab.com:cmckendry/secrets.git ~/Configurations/secrets > /dev/null
        source ~/Configurations/secrets/.secrets
    fi

    if [ ! -f /var/db/ntp-kod ]; then
        zsh -c "$SUDO touch /var/db/ntp-kod"
        zsh -c "$SUDO chmod 666 /var/db/ntp-kod"
    fi
    zsh -c "${SUDO} sntp -sS pool.ntp.org"
fi

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

# Needed for VCL (and ffmpeg?) to handle subtitles correctly on macOS
if [[ "$unamestr" == 'Darwin' ]]; then
    launchctl setenv FONTCONFIG_PATH /opt/X11/lib/X11/fontconfig
fi

# Cargo
export PATH="$HOME/.cargo/bin:$PATH"

# NVM
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" && nvm use stable # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Pyenv
[ -f /usr/local/bin/pyenv ] && eval "$(pyenv init -)"

export PATH="/usr/local/opt/curl-openssl/bin:$PATH"
export PATH="/usr/local/opt/openssl@1.1/bin:$PATH"

which fastly > /dev/null && eval "$(fastly --completion-script-zsh)"

# tabtab source for packages
# uninstall by removing these lines
[[ -f ~/.config/tabtab/__tabtab.zsh ]] && . ~/.config/tabtab/__tabtab.zsh || true

# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[[ -f /Users/carter/projects/dibs-url-shortener/node_modules/tabtab/.completions/serverless.zsh ]] && . /Users/carter/projects/dibs-url-shortener/node_modules/tabtab/.completions/serverless.zsh
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[[ -f /Users/carter/projects/dibs-url-shortener/node_modules/tabtab/.completions/sls.zsh ]] && . /Users/carter/projects/dibs-url-shortener/node_modules/tabtab/.completions/sls.zsh
# tabtab source for slss package
# uninstall by removing these lines or running `tabtab uninstall slss`
[[ -f /Users/carter/projects/dibs-url-shortener/node_modules/tabtab/.completions/slss.zsh ]] && . /Users/carter/projects/dibs-url-shortener/node_modules/tabtab/.completions/slss.zsh
# Created by `pipx` on 2022-09-11 00:40:30
export PATH="$PATH:/Users/carter/.local/bin"

function gam() { "/Users/carter/bin/gam/gam" "$@" ; }

alias gam="/Users/carter/bin/gam/gam"
