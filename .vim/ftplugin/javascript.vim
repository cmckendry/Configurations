" Settings specific to JS files

setlocal shiftwidth=2 
setlocal tabstop=2
setlocal softtabstop=2

autocmd BufWritePre <buffer> ALEFix eslint
