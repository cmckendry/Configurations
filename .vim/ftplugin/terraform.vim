" Settings specific to Terraform files

autocmd BufWritePre <buffer> ALEFix terraform
autocmd BufWritePre <buffer> :keeppatterns g/^}/-1s/^$\n//|''
